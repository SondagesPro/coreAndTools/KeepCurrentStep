<?php

/**
 * keep current step
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class KeepCurrentStep extends PluginBase
{

    protected static $name = 'keepCurrentStep';
    protected static $description = 'Keep current step when reload (save current page to lastpage when show question.';

    protected $storage = 'DbStorage';
    /**
     * @var array[] the settings
     */
    protected $settings = [
        'active' => [
            'type' => 'boolean',
            'label' => 'Activate by default on all surveys',
            'help' => 'When activated, the current page was saved yo lastpage when show the fist question in each page. The lastpage saved was the page set when reloading survey.',
            'default' => false
        ],
    ];
    /**
    * Add function to be used in beforeQuestionRender event and to attriubute
    */
    public function init()
    {
        /* The action */
        $this->subscribe('beforeQuestionRender', 'saveCurrentStep');

        // The survey seeting
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
    }

    /** Add the settings */
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('survey');
        $defaultString = $this->translate("No");
        if ($this->get('active', null, null, false)) {
            $defaultString = $this->translate("Yes");
        }
        $this->getEvent()->set("surveysettings.{$this->id}", [
            'name' => get_class($this),
            'settings' => [
                'active' => [
                    'type' => 'select',
                    'label' => $this->translate('Save current step when show question.'),
                    'options' => [
                        'N' => $this->translate("No"),
                        'Y' => $this->translate("Yes"),
                    ],
                    'htmlOptions' => [
                        'empty' => sprintf($this->translate("Default (%s)"), $defaultString),
                    ],
                    'current' => $this->get('active', 'Survey', $surveyId, ''),
                ],
            ],
        ]);
    }

    /** Save the settings */
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        foreach ($oEvent->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $oEvent->get('survey'), '');
        }
    }

    /**
    * Savec the current step when show question
    * use beforeRenderQuestion event
    */
    public function saveCurrentStep()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->getEvent();
        $surveyId = $oEvent->get('surveyId');
        $this->unsubscribe('beforeQuestionRender');
        $isSurveyActive = $this->get('active', 'Survey', $surveyId, '');
        if ($isSurveyActive == 'N') {
            return;
        }
        if ($isSurveyActive === '' && !$this->get('active', null, null, false)) {
            return;
        }
        if (!Survey::model()->findByPk($surveyId)->getIsActive()) {
            return;
        }
        if (empty($_SESSION['survey_' . $surveyId]['srid'])) {
            return;
        }
        $srid = $_SESSION['survey_' . $surveyId]['srid'];
        $step = 0;
        if (isset($_SESSION['survey_' . $surveyId]['step'])) {
            $step = $_SESSION['survey_' . $surveyId]['step'];
        }
        Response::model($surveyId)->updateByPk($srid, ['lastpage' => $step]);
        if (isset($_SESSION['survey_' . $surveyId]['scid'])) {
            SavedControl::model()->updateByPk($_SESSION['survey_' . $surveyId]['scid'], ['saved_thisstep' => $step]);
        }
    }

    /**
     * @see parent::gT for LimeSurvey 3.0
     *
     * @param string $sToTranslate The message that are being translated
     * @param string $sEscapeMode  unescaped by default
     * @param string $sLanguage    use current language if is null
     *
     * @return string
     */
    private function translate($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if (is_callable($this, 'gT')) {
            return $this->gT($sToTranslate, $sEscapeMode, $sLanguage);
        }
        return $sToTranslate;
    }
}
