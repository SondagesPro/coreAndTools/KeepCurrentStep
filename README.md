# keepCurrentStep : Allow to add easily script to question. #

When limesurvey allow reloading of page, reload is done at the just previous step. With this plugin : after reloading, particpant get the real last page (with quetsion) shown.

The plugin save the current page when display question.

## Installation

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/keepCurrentStep directory

### Via ZIP dowload
- Get the file
- Extract
- Move the directory to plugins/keepCurrentStep directory inside LimeSurvey

## Contribute and issue

Contribution are welcome, for patch and issue : use gitlab.


## Copyright
- Copyright © 2023 Denis Chenu / SondagesPro <https://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
